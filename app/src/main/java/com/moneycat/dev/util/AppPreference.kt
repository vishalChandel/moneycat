package com.moneycat.dev.util

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences

class AppPreference {

    // - - Shared Preferences keys and mode
    private val PREF_NAME = "App_PREFERENCE"
    val MODE = Context.MODE_PRIVATE


    // - - Write the Boolean Value
    fun writeBoolean(
        context: Context,
        key: String?,
        value: Boolean
    ) {
        getEditor(context).putBoolean(key, value).apply()
    }

    // - - Read the Boolean Value
    fun readBoolean(
        context: Context,
        key: String?,
        defValue: Boolean
    ): Boolean {
        return getPreferences(context).getBoolean(key, defValue)
    }

    // - - Write the Integer Value
    fun writeInteger(
        context: Context,
        key: String,
        value: Int
    ) {
        getEditor(context).putInt(key, value).apply()
    }

    // - - Read the Integer Value
    fun readInteger(
        context: Context,
        key: String,
        defValue: Int
    ): Int {
        return getPreferences(context).getInt(key, defValue)
    }

    // - - Write the String Value
    fun writeString(
        context: Context,
        key: String?,
        value: Int
    ) {
        getEditor(context).putString(key, value.toString()).apply()
    }

    // - - Read the String Value
    fun readString(
        context: Activity,
        key: String?,
        defValue: String?
    ): String? {
        return getPreferences(context).getString(key, defValue)
    }

    // - - Write the Float Value
    fun writeFloat(
        context: Context,
        key: String?,
        value: Float
    ) {
        getEditor(context).putFloat(key, value).apply()
    }

    // - - Read the Float Value
    fun readFloat(
        context: Context,
        key: String?,
        defValue: Float
    ): Float {
        return getPreferences(context).getFloat(key, defValue)
    }

    // - - Write the Long Value
    fun writeLong(
        context: Context,
        key: String?,
        value: Long
    ) {
        getEditor(context).putLong(key, value).apply()
    }

    // - - Read the Long Value
    fun readLong(
        context: Context,
        key: String?,
        defValue: Long
    ): Long {
        return getPreferences(context).getLong(key, defValue)
    }


    // - - Return the SharedPreferences with Preference Name & Preference MODE
    fun getPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(PREF_NAME, MODE)
    }

    // - - Return the SharedPreferences Editor
    private fun getEditor(context: Context): SharedPreferences.Editor {
        return getPreferences(context).edit()
    }

}