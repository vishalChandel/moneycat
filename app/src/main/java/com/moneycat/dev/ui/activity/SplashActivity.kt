package com.moneycat.dev.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.moneycat.dev.R
import com.moneycat.dev.util.SPLASH_TIME_OUT

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setUpSplash()
    }

    private fun setUpSplash() {
        val mThread = object : Thread() {
            override fun run() {
                sleep(SPLASH_TIME_OUT.toLong())
                val i = Intent(this@SplashActivity, HomeActivity::class.java)
                startActivity(i)
                finish()
            }
        }
        mThread.start()
    }
}