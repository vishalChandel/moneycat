package com.moneycat.dev.ui.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.moneycat.dev.R
import com.moneycat.dev.slotImageScroll.EventEnd
import com.moneycat.dev.ui.activity.HomeActivity
import com.moneycat.dev.util.AppPreference
import com.moneycat.dev.util.REWARD_POINTS
import kotlinx.android.synthetic.main.fragment_home2.*
import kotlinx.android.synthetic.main.fragment_home2.view.*
import kotlin.random.Random


class HomeFragment2 : BaseFragment(), EventEnd {

    // - - Initialize Objects
    var countDown = 0
    var mPoints: Int = 2500
    var mSelected = 0
    var mLastClickTime: Long = 0
    var mMediaPlayer: MediaPlayer? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_home2, container, false)
        view.image1.setEventEnd(this@HomeFragment2)
        view.image2.setEventEnd(this@HomeFragment2)
        view.image3.setEventEnd(this@HomeFragment2)
        view.imgSpinIV.setOnClickListener {
            if (isValidate()) {
                playAudio(R.raw.machine_running)
                // Preventing multiple clicks, using threshold of 2.5 second
                if (SystemClock.elapsedRealtime() - mLastClickTime < 2500) {
                    return@setOnClickListener
                }
                mLastClickTime = SystemClock.elapsedRealtime()
                image1.setRandomValue(Random.nextInt(6), Random.nextInt(15 - 5 + 1) + 5)
                image2.setRandomValue(Random.nextInt(6), Random.nextInt(15 - 5 + 1) + 5)
                image3.setRandomValue(Random.nextInt(6), Random.nextInt(15 - 5 + 1) + 5)
            }
        }
        view.imgAddIV.setOnClickListener {
            showAlertDialog(activity, getString(R.string.you_have_added_500_coins))
            mPoints += 500
            AppPreference().writeInteger(requireActivity(), REWARD_POINTS, mPoints)
            txtPointsTV.text = mPoints.toString()
        }
        view.imgInfoIV.setOnClickListener {
            val intent = Intent(activity, HomeActivity::class.java)
            activity?.startActivity(intent)
            activity?.finish()
        }
        view.imgOneIV.setOnClickListener {
            mSelected = 1
            imgOneIV.setImageResource(R.drawable.ic_one)
            imgTwoIV.setImageResource(R.drawable.ic_two_un)
            imgMaxIV.setImageResource(R.drawable.ic_max_un)
        }
        view.imgTwoIV.setOnClickListener {
            mSelected = 2
            imgOneIV.setImageResource(R.drawable.ic_one_un)
            imgTwoIV.setImageResource(R.drawable.ic_two)
            imgMaxIV.setImageResource(R.drawable.ic_max_un)
        }
        view.imgMaxIV.setOnClickListener {
            mSelected = 3
            imgOneIV.setImageResource(R.drawable.ic_one_un)
            imgTwoIV.setImageResource(R.drawable.ic_two_un)
            imgMaxIV.setImageResource(R.drawable.ic_max)
        }
        return view
    }

    override fun eventEnd(result: Int, count: Int) {
        if (countDown < 2) {
            countDown++
        } else {
            countDown = 0
            Log.i("ImageValue",image1.value.toString()+":"+image2.value.toString()+":"+image3.value.toString())
            if (image1.value == image2.value && image2.value == image3.value && image3.value == 1) {
                clearSelection()
                playAudio(R.raw.won)
                showCorrectAlertDialog(activity, "Congratulations! you won \n40 coins.")
                mPoints = getRewardPoints()
                mPoints += 40
                txtPointsTV.text = mPoints.toString()
                AppPreference().writeInteger(requireActivity(), REWARD_POINTS, mPoints)
            } else if (image1.value == image2.value && image2.value == image3.value && image3.value == 2) {
                clearSelection()
                playAudio(R.raw.won)
                showCorrectAlertDialog(activity, "Congratulations! you won \n25 coins.")
                mPoints = getRewardPoints()
                mPoints += 25
                txtPointsTV.text = mPoints.toString()
                AppPreference().writeInteger(requireActivity(), REWARD_POINTS, mPoints)
            } else if (image1.value == image2.value && image2.value == image3.value && image3.value == 3) {
                clearSelection()
                playAudio(R.raw.won)
                showCorrectAlertDialog(activity, "Congratulations! you won \n10 coins.")
                mPoints = getRewardPoints()
                mPoints += 10
                txtPointsTV.text = mPoints.toString()
                AppPreference().writeInteger(requireActivity(), REWARD_POINTS, mPoints)
            } else if (image1.value == image2.value && image2.value == image3.value && image3.value == 4) {
                clearSelection()
                playAudio(R.raw.won)
                showCorrectAlertDialog(activity, "Congratulations! you won \n80 coins.")
                mPoints = getRewardPoints()
                mPoints += 80
                txtPointsTV.text = mPoints.toString()
                AppPreference().writeInteger(requireActivity(), REWARD_POINTS, mPoints)
            } else if (image1.value == image2.value && image2.value == image3.value && image3.value == 6) {
                clearSelection()
                playAudio(R.raw.won)
                showCorrectAlertDialog(activity, "Congratulations! you won \n800 coins.")
                mPoints = getRewardPoints()
                mPoints += 800
                txtPointsTV.text = mPoints.toString()
                AppPreference().writeInteger(requireActivity(), REWARD_POINTS, mPoints)
            }else if (image1.value == 5 || image2.value == 5 || image3.value == 5) {
                clearSelection()
                playAudio(R.raw.won)
                showCorrectAlertDialog(activity, "Congratulations! you won \n2 coins.")
                mPoints = getRewardPoints()
                mPoints += 2
                txtPointsTV.text = mPoints.toString()
                AppPreference().writeInteger(requireActivity(), REWARD_POINTS, mPoints)
            }else if (image1.value == 0 || image2.value == 0 || image3.value == 0) {
                clearSelection()
                playAudio(R.raw.won)
                showCorrectAlertDialog(activity, "Congratulations! you won \n2 coins.")
                mPoints = getRewardPoints()
                mPoints += 2
                txtPointsTV.text = mPoints.toString()
                AppPreference().writeInteger(requireActivity(), REWARD_POINTS, mPoints)
            }
            else {
                countDown = 0
                playAudio(R.raw.lost)
                if(mSelected==1) {
                    clearSelection()
                    showInCorrectAlertDialog(activity,"Sorry! you lost 1 coin.")
                    mPoints = getRewardPoints()
                    mPoints -= 1
                    txtPointsTV.text = mPoints.toString()
                    AppPreference().writeInteger(requireActivity(), REWARD_POINTS, mPoints)
                }else if(mSelected==2) {
                    clearSelection()
                    showInCorrectAlertDialog(activity,"Sorry! you lost 2 coins.")
                    mPoints = getRewardPoints()
                    mPoints -= 2
                    txtPointsTV.text = mPoints.toString()
                    AppPreference().writeInteger(requireActivity(), REWARD_POINTS, mPoints)
                }else if(mSelected==3) {
                    clearSelection()
                    showInCorrectAlertDialog(activity,"Sorry! you lost 3 coins.")
                    mPoints = getRewardPoints()
                    mPoints -= 3
                    txtPointsTV.text = mPoints.toString()
                    AppPreference().writeInteger(requireActivity(), REWARD_POINTS, mPoints)
                }
            }
        }
    }

    private fun isValidate(): Boolean {
        var flag = true
        if (mSelected == 0) {
            showAlertDialog(activity, getString(R.string.please_choose_any_one_bet))
            flag = false
        }
        return flag
    }

    private fun playAudio(mAudioId:Int) {
        try {
            pauseAudio()
            mMediaPlayer = MediaPlayer()
            mMediaPlayer= MediaPlayer.create(requireActivity().applicationContext, mAudioId)
            mMediaPlayer!!.start()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun pauseAudio() {
        if (mMediaPlayer != null) {
            mMediaPlayer?.pause()
            mMediaPlayer?.reset()
        }
    }
    fun clearSelection(){
        mSelected=0
        imgOneIV.setImageResource(R.drawable.ic_one_un)
        imgTwoIV.setImageResource(R.drawable.ic_two_un)
        imgMaxIV.setImageResource(R.drawable.ic_max_un)
    }

    // - - To Show InCorrect Alert Dialog
    fun showInCorrectAlertDialog(mActivity: Activity?, mText:String) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_incorrect)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val imgCatIV = alertDialog.findViewById<ImageView>(R.id.imgCatIV)
        val txtPointsTV = alertDialog.findViewById<TextView>(R.id.txtPointsTV)
        txtPointsTV.text=mText
        Glide.with(mActivity)
            .load(R.drawable.ic_cry_cat)
            .into(imgCatIV)
        alertDialog.show()
        alertDialog.setCanceledOnTouchOutside(true)
        var handler: Handler? = null
        handler = Handler()
        handler.postDelayed(Runnable {
            alertDialog.cancel()
            alertDialog.dismiss()
            pauseAudio()
        }, 4000)
    }

    // - - To Show Correct Alert Dialog
    fun showCorrectAlertDialog(mActivity: Activity?, mText:String) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_correct)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val imgCatIV = alertDialog.findViewById<ImageView>(R.id.imgCatIV)
        val txtPointsTV = alertDialog.findViewById<TextView>(R.id.txtPointsTV)
        txtPointsTV.text=mText
        Glide.with(mActivity)
            .load(R.drawable.ic_smile_cat)
            .into(imgCatIV)
        alertDialog.show()
        alertDialog.setCanceledOnTouchOutside(true)
        var handler: Handler? = null
        handler = Handler()
        handler.postDelayed(Runnable {
            alertDialog.cancel()
            alertDialog.dismiss()
            pauseAudio()
        }, 4000)

    }

    override fun onResume() {
        super.onResume()
        mPoints = getRewardPoints()
        txtPointsTV.text = mPoints.toString()
    }
}