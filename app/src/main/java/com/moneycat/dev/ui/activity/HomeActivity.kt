package com.moneycat.dev.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.moneycat.dev.R
import com.moneycat.dev.adapter.ViewPagerAdapter
import com.moneycat.dev.ui.fragment.HomeFragment1
import com.moneycat.dev.ui.fragment.HomeFragment2
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setupViewPager(viewPager)
    }

    private fun setupViewPager(viewpager: ViewPager) {
        val adapter: ViewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(HomeFragment1(), "")
        adapter.addFragment(HomeFragment2(), "")

        // setting adapter to view pager.
        viewpager.adapter = adapter
    }
}