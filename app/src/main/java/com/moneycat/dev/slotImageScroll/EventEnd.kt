package com.moneycat.dev.slotImageScroll

interface EventEnd {

    fun eventEnd(result: Int, count: Int)

}
