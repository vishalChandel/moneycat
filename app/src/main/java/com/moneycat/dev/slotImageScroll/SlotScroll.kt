package com.moneycat.dev.slotImageScroll

import android.animation.Animator
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import com.moneycat.dev.R
import kotlinx.android.synthetic.main.slot_image_scroll.view.*

class SlotScroll: FrameLayout {

     internal var eventEnd: EventEnd?=null
    internal var lastResult = 1
    internal var oldValue = 1

    companion object {
        private const val ANIMATION_DURATION = 150
    }

    val value: Int
        get() = Integer.parseInt(currentImage.tag.toString())

    fun setEventEnd(eventEnd: EventEnd){
        this.eventEnd = eventEnd
    }

    constructor(context: Context) : super(context){
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs){
        init(context)
    }

    private fun init(context: Context){
        LayoutInflater.from(context).inflate(R.layout.slot_image_scroll, this)
        nextImage.translationY = height.toFloat()
    }

    fun setRandomValue(image:Int, numRoll:Int){
        currentImage.animate()
            .translationY(-height.toFloat())
            .setDuration(ANIMATION_DURATION.toLong()).start()

        nextImage.translationY = nextImage.height.toFloat()
        nextImage.animate()
            .translationY(0f).setDuration(ANIMATION_DURATION.toLong())
            .setListener(object: Animator.AnimatorListener{
                override fun onAnimationRepeat(p0: Animator?) {
                }

                override fun onAnimationEnd(p0: Animator?) {
                    setImage(currentImage, oldValue%6)
                    currentImage.translationY = 0f
                    if(oldValue != numRoll) {
                        setRandomValue(image,numRoll)
                        oldValue++
                    }
                    else {
                        lastResult = 1
                        oldValue = 1
                        setImage(nextImage, image)
                        eventEnd!!.eventEnd(image%6, numRoll)

                    }

                }

                override fun onAnimationCancel(p0: Animator?) {
                }

                override fun onAnimationStart(p0: Animator?) {
                }

            }).start()

    }

    //!! symbol is for asserting non-null to variables
    private fun setImage(currentImage: ImageView?, value: Int){
        when (value) {
            Utils.redBar -> currentImage!!.setImageResource(R.drawable.ic_red_bar)
            Utils.greenBar -> currentImage!!.setImageResource(R.drawable.ic_green_bar)
            Utils.gradientBar -> currentImage!!.setImageResource(R.drawable.ic_gradient_bar)
            Utils.cycle -> currentImage!!.setImageResource(R.drawable.ic_cycle)
            Utils.chinese -> currentImage!!.setImageResource(R.drawable.ic_chinese)
            Utils.cat -> currentImage!!.setImageResource(R.drawable.ic_cat)
        }
        currentImage!!.tag = value
        lastResult = value
    }



}